package kata19_WordChains;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class WordChains {
	/**
	 * Loads dictionary from file and tries to transform start word to target word. In each step only one char can
	 * be modified.
	 * 
	 * @param args[0]
	 *            path to dictionary
	 * @param args[1]
	 *            word to begin with
	 * @param args[2]
	 *            target word
	 */
	public static void main(String[] args) {
		long startTime = System.currentTimeMillis();
		String path = null;
		String startWord = null;
		String endWord = null;
		if (args.length == 0) {
			//if args are empty use defaults
			path = "dicts\\wordlist.txt";
			startWord = "CAT";
			endWord = "FROG";
		} else {
			// if possible load params from args
			path = args[0];
			startWord = args[1];
			endWord = args[2];
		}
		Set<String> dict;
		try {
			dict = readFile(path);
		} catch (FileNotFoundException e) {
			System.out.println("File " + path + " not found. Please ensure that the specified file exists.");
			// TODO print info about usage
			return;
		}
		if (dict == null) {
			// TODO print info about usage
			return;
		}
		long dictLoaded = System.currentTimeMillis();
		List<String> result = findSolution(dict, startWord, endWord);
		long solutionFound = System.currentTimeMillis();
		for (String line : result) {
			System.out.println(line);
		}
		System.out.println("dict loaded in " + (dictLoaded - startTime) + "[ms]");
		System.out.println("solution found in " + (solutionFound - dictLoaded) + "[ms]");
		System.out.println("total execution time " + (solutionFound - startTime) + "[ms]");
	}

	/**
	 * searches solution for riddle.
	 * @param dict dictionary with all words which can be used to solve riddle
	 * @param startWord word to begin with
	 * @param endWord this word is a target
	 * @return List with solution if such exists.
	 */
	static List<String> findSolution(Set<String> dict, String startWord, String endWord) {
		int treeLevel = 0;
		//initialize trees roots
		Map<String, String> startWordTree = new HashMap<String, String>();
		Map<String, String> endWordTree = new HashMap<String, String>();
		startWordTree.put(startWord, null);
		endWordTree.put(endWord, null);
		
		//set roots as unchecked
		List<String> startWordUncheckedNodes = new ArrayList<String>();
		List<String> endWordUncheckedNodes = new ArrayList<String>();
		startWordUncheckedNodes.add(startWord);
		endWordUncheckedNodes.add(endWord);

		Set<String> startDict = new HashSet<String>(dict);
		Set<String> endDict = new HashSet<String>(dict);
		
		//remove start word from dict to prevent creation of loops in tree
		startDict.remove(startWord);

		String middleNode = null;
		//main search loop
		search: while (true) {
			treeLevel++;
			System.out.println("treeLevel: " + treeLevel);
			List<String> newStartWordUncheckedNodes = new ArrayList<String>();
			//try to create leafs for unchecked nodes
			for (String node : startWordUncheckedNodes) {
				List<String> neightbours = findNeightbors(node, startDict, endWord);
				for (String newNodeName : neightbours) {
					startWordTree.put(newNodeName, node);
					newStartWordUncheckedNodes.add(newNodeName);
					//if end word found (possible in first pass) or common leaf with second tree found then end search. 
					if (newNodeName.equals(endWord) || endWordTree.containsKey(newNodeName)) {
						middleNode = newNodeName;
						break search;
					}
				}
			}
			//if tree cant grow (new leafs not found) finding solution is impossible
			if (newStartWordUncheckedNodes.isEmpty()) {
				break search;
			}
			if (treeLevel == 1) {
				//remove end word from dict to prevent creation of loops in tree
				endDict.remove(endWord);
			}
			//new leafs become unchecked nodes
			startWordUncheckedNodes = newStartWordUncheckedNodes;
			
			List<String> newEndWordUncheckedNodes = new ArrayList<String>();
			//try to create leafs for unchecked nodes
			for (String node : endWordUncheckedNodes) {
				List<String> neightbours = findNeightbors(node, endDict, startWord);
				for (String newNodeName : neightbours) {
					endWordTree.put(newNodeName, node);
					newEndWordUncheckedNodes.add(newNodeName);
					//if end word found (possible in first pass) or common leaf with second tree found then end search. 
					if (newNodeName.equals(startWord) || startWordTree.containsKey(newNodeName)) {
						middleNode = newNodeName;
						break search;
					}
				}
			}
			//if tree cant grow (new leafs not found) finding solution is impossible
			if (newStartWordUncheckedNodes.isEmpty()) {
				break search;
			}
			//new leafs become unchecked nodes
			endWordUncheckedNodes = newEndWordUncheckedNodes;
		}
		//format output for easy print
		List<String> result = new ArrayList<String>();

		if (middleNode == null) {
			result.add("can't find way to transform word.");
		} else {
			result.add("solution:");
			String printNode = middleNode;
			List<String> list = new ArrayList<String>();
			while (true) {
				if ((printNode = startWordTree.get(printNode)) == null) {
					break;
				}
				list.add(printNode);
			}
			for (int i = 0; i < list.size(); i++) {
				result.add(list.get(list.size() - i - 1));
			}
			printNode = middleNode;
			while (true) {
				result.add(printNode);
				if ((printNode = endWordTree.get(printNode)) == null) {
					break;
				}
			}
		}
		return result;
	}
/**
 * searches dictionary for words that can be created from root word. If root word can be modified in single step to target word then returns only target word.
 * @param rootWord
 * @param dictionary
 * @param targetWord
 * @return list with all words which can be created from rootWord.
 */
	static List<String> findNeightbors(String rootWord, Set<String> dictionary, String targetWord) {
		List<String> neightbours = new ArrayList<String>();
		for (String newNodeName : dictionary) {
			if (areTransformableToEachOther(newNodeName, rootWord)) {
				if (targetWord.equals(newNodeName)) {
					neightbours = new ArrayList<String>();
					neightbours.add(newNodeName);
					break;
				}
				neightbours.add(newNodeName);
			}
		}
		//trim dictionary to prevent creation of loops in tree
		dictionary.removeAll(neightbours);
		return neightbours;
	}
/**
 * @param a
 * @param b
 * @return true if word 'a' can be transformed into word 'b' else returns false
 */
	static boolean areTransformableToEachOther(String a, String b) {
		int lengthDiff = a.length() - b.length();
		//esure that a is longer than b
		if (lengthDiff == -1) {
			String tmp = a;
			a = b;
			b = tmp;
			lengthDiff = 1;
		}
		if (lengthDiff == 0) {
			int charsDiffCount = 0;
			int index = 0;
			while (index < a.length() && charsDiffCount < 2) {
				if (a.charAt(index) != b.charAt(index)) {
					charsDiffCount++;
				}
				index++;
			}
			if (charsDiffCount < 2) {
				return true;
			}
		} else if (lengthDiff == 1) {
			int charsDiffCount = 0;
			int indexA = 0;
			int indexB = 0;
			while (indexA < a.length() && indexB < b.length() && charsDiffCount < 2) {
				if (a.charAt(indexA) != b.charAt(indexB)) {
					indexA++;
					charsDiffCount++;
					continue;
				}
				indexA++;
				indexB++;
			}
			if (charsDiffCount < 2) {
				return true;
			}
		}
		return false;
	}
/**
 * 
 * @param path file Path
 * @return set with all words from given file. Duplicates are removed. All words are uppercase.
 * @throws FileNotFoundException
 */
	static Set<String> readFile(String path) throws FileNotFoundException {
		BufferedReader inputStream = null;

		inputStream = new BufferedReader(new FileReader(path));

		Set<String> set = new HashSet<String>();
		try {
			String line = null;
			while ((line = inputStream.readLine()) != null)
				set.add(line.toUpperCase());
		} catch (IOException e) {
			System.out.println("error reading File");
		} finally {
			System.out.println("closing file");
			try {
				inputStream.close();
				System.out.println("file closed succesfully");
			} catch (IOException e) {
				System.out.println("closing file failed!");
			}
		}
		return set;
	}
}
