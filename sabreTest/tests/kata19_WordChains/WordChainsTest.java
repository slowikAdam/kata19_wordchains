package kata19_WordChains;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

public class WordChainsTest {
	
	@Test
	public void testMain() {
			ByteArrayOutputStream outContent = new ByteArrayOutputStream();
			PrintStream tmp=System.out;
			

		    System.setOut(new PrintStream(outContent));
		    String[] args= {"dicts\\wordlist.txt","CAT","DOG"};
		    WordChains.main(args);
		    System.setOut(tmp);
		    String result=outContent.toString();
		    String expResult=outContent.toString();
		    expResult="closing file\r\n" + 
		    		"file closed succesfully\r\n" + 
		    		"treeLevel: 1\r\n" + 
		    		"treeLevel: 2\r\n" + 
		    		"solution:\r\n" + 
		    		"CAT\r\n" + 
		    		"COT\r\n" + 
		    		"COG\r\n" + 
		    		"DOG";
		    assertTrue(result.startsWith(expResult));
		   
	}


	@Test
	public void testFindSolutionBasicTest() {
		Set<String> dict = new HashSet<String>();
		dict.add("CAT");
		dict.add("COT");
		dict.add("COG");
		dict.add("DOG");
		List<String> result = WordChains.findSolution(dict, "CAT", "DOG");
		List<String> expResult = new ArrayList<String>();
		expResult.add("solution:");
		expResult.add("CAT");
		expResult.add("COT");
		expResult.add("COG");
		expResult.add("DOG");
		assertEquals(expResult, result);
	}

	@Test
	public void testFindSolutionCantFindTooShortDict() {
		Set<String> dict = new HashSet<String>();
		dict.add("CAT");
		List<String> result = WordChains.findSolution(dict, "CAT", "DOG");
		List<String> expResult = new ArrayList<String>();
		expResult.add("can't find way to transform word.");
		assertEquals(expResult, result);
	}

	@Test
	public void testFindNeightborsBasicTest() {
		Set<String> dict = new HashSet<String>();
		dict.add("CATT");
		dict.add("CATTT");
		dict.add("CA");
		dict.add("CAD");
		dict.add("COO");
		Set<String> result = new HashSet<String>();
		result.addAll(WordChains.findNeightbors("CAT", dict, "DOG"));
		Set<String> expResult = new HashSet<String>();
		expResult.add("CATT");
		expResult.add("CA");
		expResult.add("CAD");
		assertEquals(expResult, result);
	}

	@Test
	public void testFindNeightborsEmptyResult() {
		Set<String> dict = new HashSet<String>();
		dict.add("CATT");
		dict.add("CATTT");
		dict.add("CA");
		dict.add("CAD");
		dict.add("COO");
		Set<String> result = new HashSet<String>();
		result.addAll(WordChains.findNeightbors("DOG", dict, "FROG"));
		assertTrue(result.isEmpty());
	}

	@Test
	public void testFindNeightborsTargetWord() {
		Set<String> dict = new HashSet<String>();
		dict.add("CATT");
		dict.add("CATTT");
		dict.add("CA");
		dict.add("CAD");
		dict.add("COO");
		Set<String> result = new HashSet<String>();
		result.addAll(WordChains.findNeightbors("CAT", dict, "CAD"));
		Set<String> expResult = new HashSet<String>();
		expResult.add("CAD");
		assertEquals(expResult, result);
	}

	@Test
	public void testAreTransformableToEachOtherScenario1() {
		assertTrue(WordChains.areTransformableToEachOther("CAT", "CATT"));
	}

	@Test
	public void testAreTransformableToEachOtherScenario2() {
		assertFalse(WordChains.areTransformableToEachOther("CAT", "CATTT"));
	}

	@Test
	public void testAreTransformableToEachOtherScenario3() {
		assertTrue(WordChains.areTransformableToEachOther("CAT", "CAAT"));
	}

	@Test
	public void testAreTransformableToEachOtherScenario4() {
		assertFalse(WordChains.areTransformableToEachOther("CAT", "DOG"));
	}

	@Test(expected = FileNotFoundException.class)
	public void testReadFileWhenFileDontExistsExceptionIsThrown() throws FileNotFoundException {
		WordChains.readFile("C:::");
	}

	@Test
	public void testReadFileSimpleFileReadTest() throws FileNotFoundException {
		Set<String> result = WordChains.readFile("dicts\\test_dict.txt");
		Set<String> expResult = new HashSet<>();
		expResult.add("DOG");
		expResult.add("CAT");
		assertEquals(expResult, result);
	}

	@Test
	public void testReadFileAreDuplicatesRemoved() throws FileNotFoundException {
		Set<String> result = WordChains.readFile("dicts\\test_dict2.txt");
		Set<String> expResult = new HashSet<>();
		expResult.add("DOG");
		expResult.add("CAT");
		assertEquals(expResult, result);
	}

}
